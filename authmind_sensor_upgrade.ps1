<#

Script: AuthMind Sensor Upgrade Script
Author: Michael J. Acosta
Date: Oct 20 2022
Description:
    To make future updates quicker to install to all DC's, upload the msi package to AWS (bit.ly shortcuts are blocked in the org) and then update the $MSI and $URI variables and run the script to mass deploy/upgrade.
    At the end of the installation, it will run PortQry to confirm communication with console.authmind.com
#>

 # TIP : Store all Domain Controllers that have the AuthMind sensor installed in a variable and use the ForEach-Object cmdlet to iteract trough each DCs
Write-Host "Retrieving all Domain Controllers that have AuthMind Installed..."
$DCs = Get-ADDomainController -Filter * | ForEach-Object {Invoke-Command -ScriptBlock {Get-WmiObject -Class Win32_Product | Where-Object Name -eq "AuthMind AD Sensor" } -ComputerName $_.Name -ErrorAction SilentlyContinue}

Write-Host "Downloading, Installing AuthMind and Confirming connection to console.authmind.com"
$DCs | ForEach-Object `
{
Invoke-Command -ScriptBlock {


  # AuthMind Variables (Update the $MSI and $URI as needed)
  $AuthMindMSI="AuthMind+AD+Sensor-1.3.9-MSI-Setup.msi";
  $AuthMindURI="https://steve-madden-ltd.s3.amazonaws.com/Windows/AuthMind/AuthMind%2BAD%2BSensor-1.3.9-MSI-Setup.msi"

  # Temp install Path
  $AuthMindPath="C:\tmp\";
  $AuthMind = $AuthMindPath + $AuthMindMSI;

 
  function sensorInstall {
     
    $CurrentAuthMind = Test-Path -Path $AuthMind -ErrorAction SilentlyContinue;
      if ($CurrentAuthMind) {
          Write-Host "AuthMind installer found, proceeding with install...";
          stop-service authmind -ErrorAction SilentlyContinue

          $AuthMindWin32Product = Get-WmiObject -Class Win32_Product | Where-Object Name -eq "AuthMind AD Sensor"

          if ($AuthMindWin32Product) {
            $AuthMindWin32Product.Uninstall() | Out-Null
          }
          
          
          msiexec.exe /I $AuthMind /quiet
          $AuthMindSensorVersion = Get-WmiObject -Class Win32_Product | Where-Object Name -eq "AuthMind AD Sensor"
              Write-Host "Sensor Upgrade Completed:"
              Write-Host $AuthMindSensorVersion

              }
          else {
              Write-Host "Downloading $AuthMindMSI...";
              Remove-Item -Path $AuthMindPath -Recurse -Force -ErrorAction SilentlyContinue;
              New-Item -Path $AuthMindPath -ItemType Directory;
              Invoke-WebRequest -Uri $AuthMindURI -OutFile $AuthMind;
              stop-service authmind -ErrorAction SilentlyContinue
              $AuthMindWin32Product = Get-WmiObject -Class Win32_Product | Where-Object Name -eq "AuthMind AD Sensor"
              
              if ($AuthMindWin32Product) {
                $AuthMindWin32Product.Uninstall() | Out-Null
              }
          
              
              msiexec.exe /I $AuthMind /quiet
              $AuthMindSensorVersion = Get-WmiObject -Class Win32_Product | Where-Object Name -eq "AuthMind AD Sensor"
              Write-Host "Sensor Upgrade Completed:"
              Write-Host $AuthMindSensorVersion


        };


  }

  

  # Let's confirm that the DC can contact console.authmind.com on port 80 > 443 (redirection)
  $PortQueryPath="C:\PortQry\";
  $Exec="PortQry.exe";
  $PortQry = $PortQueryPath + $Exec;
  $PortQryURI="https://steve-madden-ltd.s3.amazonaws.com/Windows/PortQry/PortQry.exe "
  function portCheck{
      $CurrentPortQry = Test-Path -Path $PortQry -ErrorAction SilentlyContinue;
      if ($CurrentPortQry) {
          Write-Host "PortQry Found, checking ports...";
          $PortQry;
              }
          else {
              Write-Host "No previous version of PortQry found. Installing...";
              Remove-Item -Path $PortQueryPath -Recurse -Force -ErrorAction SilentlyContinue;
              New-Item -Path $PortQueryPath -ItemType Directory;
              Invoke-WebRequest -Uri $PortQryURI -OutFile $PortQry;
        };
  };
  function portEnum{
  
  
      & $PortQry @('-n','console.authmind.com','-e','80');
      & $PortQry @('-n','console.authmind.com','-e','443');
  
  
  };


  sensorInstall
  portCheck;
  portEnum;
  hostname
  
  
  } -ComputerName $_.PSComputerName }

  Write-Host "Process Done!"
  exit 0
  
  

